# dtrc - Dmenu Transmission Remote Control

Simple POSIX script for managing torrents from dmenu.

<img src="https://i.ibb.co/YNJPM8Q/dtrc-01.png" alt="dtrc-01" border="0">

<img src="https://i.ibb.co/pvx3CMd/dtrc-02.png" alt="dtrc-02" border="0">

**Depenencies:** transmission-cli, dmenu

**Installation:** Just copy to your bin folder. E.g. `sudo cp dtrc /usr/local/bin`

\* make sure it is executable `chmod +x filename`

**ToDo**
*  speed control (alt-speed)
*  refresh
*  show torrent info
*  manual set download directory